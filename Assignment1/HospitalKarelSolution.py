from karel.stanfordkarel import *

# File: HospitalKarel.py
# -----------------------------
# Here is a place to program your section problem
# Precondition: state of the world before your function call
# Postcondition: state of the world after your function call

def build_hospital():
    """
    Helper function to build a "hospital" (2 columns of 3 beepers). Karel will start facing East at the bottom left corner of the hospital and end facing East at the bottom right corner of the hospital.
    """
    # make first column
    turn_left()
    place_row()
    # move to place the second column
    turn_right()
    move()
    turn_right()
    # build second column and turn to face East again
    place_row()
    turn_left()

def place_row():
    """
    Helper function to put 3 beepers in a row starting at the current position and going in the direction Karel is facing.
    """
    put_beeper()
    for i in range(2):
        move()
        put_beeper()

def turn_right():
    for i in range(3):
        turn_left()

def main():
    while front_is_clear():
        if beepers_present():
            pick_beeper()
            build_hospital()
        if front_is_clear():
            # if there isn't a wall immediately to the right of the hospital, we need to move forward so that in the next iteration of the loop, Karel isn't standing on a beeper (which is actually part of the hospital we just built!)
            move()


if __name__ == '__main__':
    run_karel_program()
