from karel.stanfordkarel import *

# File: HospitalKarelSection.py
# -----------------------------
# Code written in Section 1

def main():
    # find to the location
    # need to not progress if path nto clear
    while front_is_clear():
        if beepers_present():
            build_hospital()
        if front_is_clear():
            move()

    # build hospital
def build_hospital():
    turn_left()
    pick_beeper()
    # Build the hospital column
    build_column()
    # turn around and move over
    turn_right()
    move()
    turn_right()
    # build second column
    build_column()
    turn_left()

def build_column():
    put_beeper()
    move()
    put_beeper()
    move()
    put_beeper()

def turn_right():
    for i in range(3):
        turn_left()


if __name__ == '__main__':
    run_karel_program()
